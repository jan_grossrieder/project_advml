-- Script implementing a convolutional audio-LSTM.
-- Audio features need to be stored in '../python/features/audio_feat.h5'
-- and video features in '../python/features/im_gray_feat.h5'.
--
-- Maintainer:    Jan Grossrieder
-- Last modified: 30th of May 2017

require 'cunn'
require 'optim'
require 'LSTM'
require 'hdf5'
require 'cutorch'
require 'cudnn'

--------------------Definitions-----------------------------------------
Epochs = 5

adam_config = {
    learningRate = 0.001,
    learningRateDecay = 0,
    beta1 = 0.9,
    beta2 = 0.999,
    epsilon = 1e-8,
    weightDecay = 0,
}

batchSz = 64

--------------------Model-----------------------------------------
-- Create encoder
encoder = nn.Sequential()
encoder:add(nn.SpatialConvolution(1,32,3,3,1,1,1,0))
encoder:add(nn.ReLU())
encoder:add(nn.SpatialMaxPooling(2,2,1,2,0,0))
encoder:add(nn.SpatialConvolution(32,64,3,3,1,1,1,0))
encoder:add(nn.ReLU())
encoder:add(nn.SpatialMaxPooling(2,2,1,2,0,0))
encoder:add(nn.SpatialConvolution(64,64,3,3,1,1,1,0))
encoder:add(nn.ReLU())
encoder:add(nn.SpatialMaxPooling(2,2,1,2,0,0))

-- Create LSTM
lstm = nn.Sequential()
-- Reshape for LSTM; N items x T sequence length x H hidden size
-- Here T is equal to 1 for one image
lstm:add(nn.View(-1,1,104))
lstm:add(nn.LSTM(104,104))
lstm:add(nn.View(64,26,4))

-- Create decoder
decoder = nn.Sequential()
decoder:add(nn.SpatialConvolution(64,64,3,3,1,1,1,1))
decoder:add(nn.ReLU())
decoder:add(nn.SpatialUpSamplingNearest(2))
decoder:add(nn.SpatialConvolution(64,64,3,3,1,1,1,1))
decoder:add(nn.ReLU())
decoder:add(nn.SpatialUpSamplingNearest(2))
decoder:add(nn.SpatialConvolution(64,32,3,3,1,1,1,1))
decoder:add(nn.ReLU())
decoder:add(nn.SpatialUpSamplingNearest(2))
decoder:add(nn.SpatialConvolution(32,1,3,3,1,1,1,1))
decoder:add(nn.ReLU())
decoder:add(nn.View(6656))
decoder:add(nn.Linear(6656, 4096))
decoder:add(nn.View(1,64,64))
decoder:add(nn.Sigmoid())

-- Create Convolutional AutoEncoder
autoencoder = nn.Sequential()
autoencoder:add(encoder)
autoencoder:add(lstm)
autoencoder:add(decoder)

-- typecast model to cuda
autoencoder = autoencoder:cuda()

-------------------------------------------------------------------------
get_nbr_train_samples = function()
    -- options are 'train' or 'test'
    local dataset = 'train'

    -- read the file
    local featFile = hdf5.open('../python/features/audio_feat.h5', 'r')

    -- get the size of the training dataset without reading it
    local sz = featFile:read(dataset):dataspaceSize()
    featFile:close()
    
    return sz[1]
end

get_nbr_test_samples = function()
    -- options are 'train' or 'test'
    local dataset = 'test'

    -- read the file
    local featFile = hdf5.open('../python/features/audio_feat.h5', 'r')

    -- get the size of the training dataset without reading it
    local sz = featFile:read(dataset):dataspaceSize()
    featFile:close()
    
    return sz[1]
end

read_train_samples = function(start_idx, end_idx)
    -- options are 'train' or 'test'
    local dataset = 'train'

    -- read the file
    local featFile = hdf5.open('../python/features/audio_feat.h5', 'r')

    -- get the size of the training dataset without reading it
    local sz = featFile:read(dataset):dataspaceSize()

    -- partial read
    local batch = featFile:read(dataset):partial({start_idx,end_idx},{1,sz[2]},{1,sz[3]},{1,sz[4]}):double()
    featFile:close()

    -- add a zero value
    batch = torch.cat(batch, torch.Tensor(batch:size()[1], batch:size()[2], 1, batch:size()[4]), 3)

    return batch:cuda()
end

read_test_samples = function(start_idx, end_idx)
    --Input
    -- options are 'train' or 'test'
    local dataset = 'test'

    -- read the file
    local featFile = hdf5.open('../python/features/audio_feat.h5', 'r')

    -- get the size of the training dataset without reading it
    local sz = featFile:read(dataset):dataspaceSize()

    -- partial read
    local batch = featFile:read(dataset):partial({start_idx,end_idx},{1,sz[2]},{1,sz[3]},{1,sz[4]}):double()
    featFile:close()

    -- add a zero value
    batch = torch.cat(batch, torch.Tensor(batch:size()[1], batch:size()[2], 1, batch:size()[4]), 3)

    return batch:cuda()
end

read_train_labels = function(start_idx, end_idx)
    -- output
    -- options are 'train' or 'test'
    local dataset = 'train'

    -- read the file
    local featFile = hdf5.open('../python/features/im_feat_gray.h5', 'r')

    -- get the size of the training dataset without reading it
    local sz = featFile:read(dataset):dataspaceSize()

    -- partial read
    local batch = featFile:read(dataset):partial({start_idx,end_idx},{1,sz[2]},{1,sz[3]},{1,sz[4]}):double()

    -- squeeze it betqeen 0 and 1
    batch = batch/255

    featFile:close()

    return batch:cuda()
end

read_test_labels = function(start_idx, end_idx)
    -- output
    -- options are 'train' or 'test'
    local dataset = 'test'

    -- read the file
    local featFile = hdf5.open('../python/features/im_feat_gray.h5', 'r')

    -- get the size of the training dataset without reading it
    local sz = featFile:read(dataset):dataspaceSize()

    -- partial read
    local batch = featFile:read(dataset):partial({start_idx,end_idx},{1,sz[2]},{1,sz[3]},{1,sz[4]}):double()

    -- squeeze it betqeen 0 and 1
    batch = batch/255

    featFile:close()

    return batch:cuda()
end

-- Create the loss
criterion = nn.MSECriterion():cuda()


-- Get parameters
parameters, gradParameters = autoencoder:getParameters()

-- Create optimiser function evaluation
local feval = function(params)
    -- Get new parameters
    if parameters ~= params then
        parameters:copy(params)
    end
    
    -- Zero gradients
    gradParameters:zero()
    -- Reset states
    lstm:clearState()
    
    -- Initialize output variables
    local loss = 0

    -- Reconstruction phase
    -- Forward propagation over all 30 frames
    for i=1, y:size()[1] do
        xTmp = x[{{i,i},{1,x:size()[2]},{1,x:size()[3]}}]
        yTmp = y[{{i,i},{1,y:size()[2]},{1,y:size()[3]}}]
        
        -- Forward propagation
        yHatTmp = autoencoder:forward(xTmp)
        loss = loss + criterion:forward(yHatTmp, yTmp)
        
        -- Backpropagation
        gradLoss = criterion:backward(yHatTmp, yTmp)
        autoencoder:backward(xTmp, gradLoss)
    end
    
    -- take mean
    loss = loss/y:size()[1]
    
    -- Return the loss and gradParameters
    return loss, gradParameters
end

-- Train

nbrTrainSamples = get_nbr_train_samples()

fd = io.open('loss.txt', 'w')
fd:write('epochs;sample;loss\n')

print('Training...')
local bestLoss = math.huge
-- iterate over epochs
for epoch = 1, Epochs do
    print('start epoch ')
    
    -- iterate over batches
    for sample = 1, nbrTrainSamples, batchSz do
        -- get last sample of batch (prevent overflow)
        tmpBatchSz = batchSz
        sample_end = sample + batchSz -1
        if sample_end > nbrTrainSamples then
            sample_end = nbrTrainSamples
            tmpBatchSz = sample_end - sample + 1
        end
        
        -- retrieve the batch
        x_batch_train = read_train_samples(sample, sample_end)
        y_batch_train = read_train_labels(sample, sample_end)
        
        -- iterate over batch samples
        for samplesInBatch = 1, tmpBatchSz do
            x = x_batch_train[{samplesInBatch,...}]:clone()
            y = y_batch_train[{samplesInBatch,...}]:clone()
            
            __,loss = optim.adam(feval, parameters, adam_config, optimState)
        end
        print('epoche: ', epoch, ', sample: ', sample, '/', nbrTrainSamples, ', loss = ', loss[1])

        -- write log file
        fd:write(epoch,';',sample,';',loss[1],'\n')
    end
    
    print('finished epoch ', epoch , '/', Epochs)
    
    -- save model if better loss
    if loss[1] < bestLoss then
        bestLoss = loss[1]
        print('--> save model...')
        torch.save('model.t7', autoencoder)
    end
end

fd:close()

print('train finished!')





-- Create optimiser function evaluation
local ftest = function()
    -- Reset states
    lstm:clearState()
    
    -- Initialize output variables
    local yHat = torch.Tensor(y:size()[1], y:size()[2], y:size()[3]):fill(0):cuda()
    local loss = 0

    -- Reconstruction phase
    -- Forward propagation over all 30 frames
    for i=1, y:size()[1] do
        xTmp = x[{{i,i},{1,x:size()[2]},{1,x:size()[3]}}]
        yTmp = y[{{i,i},{1,y:size()[2]},{1,y:size()[3]}}]
        
        -- Forward propagation, rescale it back
        yHat[{{i,i}, {1,y:size()[2]},{1,y:size()[3]}}] = autoencoder:forward(xTmp)
        loss = loss + criterion:forward(yHat[{{i,i}, {1,y:size()[2]},{1,y:size()[3]}}], yTmp)
    end
    
    -- take mean
    loss = loss/y:size()[1]
    
    -- Return the loss and gradParameters
    return loss, yHat
end


print('Testing...')
nbrTestSamples = get_nbr_test_samples()
h_file = hdf5.open("yhat.h5", "w")
-- iterate over batches
for sample = 1, nbrTestSamples, batchSz do
    -- get last sample of batch (prevent overflow)
    tmpBatchSz = batchSz
    sample_end = sample + batchSz -1
    if sample_end > nbrTestSamples then
        sample_end = nbrTestSamples
        tmpBatchSz = sample_end - sample + 1
    end
    
    -- retrieve the batch
    x_batch_train = read_test_samples(sample, sample_end)
    y_batch_train = read_test_labels(sample, sample_end)
    
    -- iterate over batch samples
    for samplesInBatch = 1, tmpBatchSz do
        x = x_batch_train[{samplesInBatch,...}]:clone()
        y = y_batch_train[{samplesInBatch,...}]:clone()
        
        loss, yHat = ftest()
        
        -- write to hdf5 file
        yHat_rescale = yHat:clone() * 255
        h_file:write(string.format("yHat_%05d", (sample + samplesInBatch - 2)), yHat_rescale:byte())
        loss_tensor = torch.FloatTensor(1)
        loss_tensor[1] = loss
        h_file:write(string.format("loss_%05d", (sample + samplesInBatch - 2)), loss_tensor)
        print('sample: ', (sample + samplesInBatch - 1), '/', nbrTestSamples, ', loss = ', loss)
    end
end

h_file:close()

print('test finished!')

