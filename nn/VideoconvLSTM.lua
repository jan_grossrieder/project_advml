require 'hdf5'
require 'nn'
require 'optim'
require 'LSTM'




--Video convLSTM  test rapidly the model
--------------------Model-----------------------------------------
-- Create encoder
encoder = nn.Sequential()
--Change for grey images channel in 3 to 1
encoder:add(nn.SpatialConvolution(1,64,3,3,1,1,1,1))
encoder:add(nn.ReLU())
encoder:add(nn.SpatialMaxPooling(2,2,2,2))
encoder:add(nn.SpatialConvolution(64,32,3,3,1,1,1,1))
encoder:add(nn.ReLU())
encoder:add(nn.SpatialMaxPooling(2,2,2,2))
encoder:add(nn.SpatialConvolution(32,32,3,3,1,1,1,1))
encoder:add(nn.ReLU())
encoder:add(nn.SpatialMaxPooling(2,2,2,2))

--Create LSTM
lstm = nn.Sequential()
 -- Reshape for LSTM; N items x T sequence length x H hidden size
 --Here T is equal to 1 for one image
lstm:add(nn.View(-1,1,32))
lstm:add(nn.LSTM(32, 32))
lstm:add(nn.View(32,8,8))
--Create decoder

decoder = nn.Sequential()
decoder:add(nn.SpatialConvolution(32,32,3,3,1,1,1,1))
decoder:add(nn.ReLU())
decoder:add(nn.SpatialUpSamplingNearest(2))
decoder:add(nn.SpatialConvolution(32,32,3,3,1,1,1,1))
decoder:add(nn.ReLU())
decoder:add(nn.SpatialUpSamplingNearest(2))
decoder:add(nn.SpatialConvolution(32,64,3,3,1,1,1,1))
decoder:add(nn.ReLU())
decoder:add(nn.SpatialUpSamplingNearest(2))
decoder:add(nn.SpatialConvolution(64,1,3,3,1,1,1,1))
decoder:add(nn.Sigmoid())

--Create Convolutional AutoEncoder
autoencoder = nn.Sequential()
autoencoder:add(encoder)
autoencoder:add(lstm)
autoencoder:add(decoder)
-------------------------------------------------------------------------
-- Set up GPU
--opt.dtype = 'torch.DoubleTensor'
--if opt.cuda == 1 then
--  require 'cunn'
--  opt.dtype = 'torch.CudaTensor'
--end

--Set the input

myFile = hdf5.open('im_feat_gray.h5','r')
videos = myFile:read('train'):all()
myFile:close()
--videos[i] --> one video
--videos[i][i] -->  Frame i of video i

--Input est égal a séquence de frame for one video
nbrVid = videos:size(1)
nbrFrame = videos:size(2)



--Weigh Decay for Regularization
weightDecay = 0.05
learningRate = 0.01
momentum = 0.9
Frame = 30
Epochs = 10

--Create the loss
criterion = nn.MSECriterion()


--Get parameters
theta, gradTheta = autoencoder:getParameters()

--Create optimiser function evaluation
--Put feval local

  --Zero gradients

  gradTheta:zero()

feval = function(params)

    --Need after for the sequencial training
    local trainLossHistory = {}
    local valLossHistory = {}
    local valLossHistoryEpochs = {}


  --Reconstruction phase
  --Forward propagation
  --Get new parameters
  if theta ~= params then
    theta:copy(params)
  end
  --Zero gradients

  gradTheta:zero()

  for j = 1, nbrFrame do
    x = video[j]:double():resize(1,64,64)

    xHat = autoencoder:forward(x) --Reconstruction
    loss = criterion:forward(xHat,x)
    --Backpropagation
    gradLoss = criterion:backward(xHat,x)
    autoencoder:backward(x, gradLoss)

    --Regularization phase
    --Weight decay
    if weightDecay > 0 then
      loss = loss + weightDecay * torch.norm(theta,2)^2/2
      gradTheta:add(theta:clone():mul(weightDecay))
    end
  end

  return loss, gradTheta
end

--Train
sgd_state = sgd_state or {
    learningRate = learningRate,
    momentum = momentum,
    learningRateDecay = 5e-7
}

print('Training')
for epoch = 1, Epochs do
  for i = 1, 30 do
    video = videos[i]
    for frame = 1, Frame do
      __, loss = optim.sgd(feval, theta, sgd_state)
      print('[Training]: Video: '..i..' Frame: '..frame..': '..loss[1])
    end
  end
  test = autoencoder:forward(videos[1][9]:double():resize(1,64,64))
  image.save('test'..epoch..'.jpg',test)
end
