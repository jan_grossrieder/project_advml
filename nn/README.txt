-----------------
VideoconvLSTM.lua
-----------------

This lua file concern the test that we perform on the video part of
the ideal convLSTM architecture as mentioned in the report.

This module allow to see if the the video part proposed can learn visual features
on given some sample examples of video frames, by testing the training part.

--Requirements
In the file you must have:
- The sample training file: im_feat_gray.h5
- The quite-canonical LSTM lua file that come from
  https://github.com/garythung/torch-lrcn/blob/master/LSTM.lua: LSTM.lua
- The main VideoconvLSTM lua file: VideoconvLSTM.lua

--Run the application:
  $ th VideoconvLSTM.lua

-- Results:
- You will see how the the training go far. By printing
  on the console the Video number, the frame number and the current
  loss of the training sample at a given frame.
- Example: "[Training]: Video: 2 Frame: 2: 492.47026823685"
- For each epoch the application will save an image of the video 1 at the
  frame number 9 after a forwarding pass.


-------------
audioLSTM.lua
-------------

This lua file contains an implementation of the Audio-convLSTM (described in
report, section 2.3.3). To run the script, the audio and video features need
to be present as '../feat/audio_feat.h5', '../feat/im_gray_feat.h5' resp.
Those datasets are very large and therefore could not be submitted through
ILIAS. That's why you might get an error while running the script.
Audio and video-features are extracted by python files in 'preprocessing'
repository.

