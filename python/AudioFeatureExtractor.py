# Extracts audio features (spectrograms) from a given audio file
# Advanced Machine Learning Project
# author: Tobias Rothen

from scipy import signal
from scipy.io import wavfile
from numpy import transpose
import math

class AudioFeatureExtractor(object):

    def __init__(self):
        self.framerate = 15
        self.hammingwindowsize = 100

    # some setters
    def setframerate(self, framerate):
        self.framerate = framerate

    def sethammingsize(self, hammingwindowsize):
        self.hammingwindowsize = hammingwindowsize

    # some getters
    # TODO

    def extractAudioClipFeatures(self, audioclip):
        """ returns audio features (spectrograms) for a given audiofile
             using object parameters: framerate, hamming window size
             input: MoviePy.AudioClip
        """
        # get soundarray from audioclip
        duration = audioclip.duration
        audioSignal = audioclip.to_soundarray(fps=44100, nbytes=2)

        # cast from stereo to mono
        if (len(audioSignal.shape) == 2):
            audioSignal = (audioSignal[:, 0] / 2) + (audioSignal[:, 1] / 2)

        # calculate sampling rate
        sampling_rate = audioSignal.size / duration

        # print('DEBUG: clip duration: ', duration)
        # print('DEBUG: audioSignal size', audioSignal.size)
        # print('DEBUG: sampling rate:', sampling_rate)

        # get dimensions
        n_samples = audioSignal.shape[0]
        audioDuration = n_samples / sampling_rate
        n_audio_segments = math.floor(audioDuration * self.framerate)
        audio_segment_length = math.floor(n_samples / n_audio_segments)

        # set up hamming window
        hamming_window_size = math.floor(sampling_rate / self.hammingwindowsize)
        hamming_window = signal.hamming(hamming_window_size)

        # get spectrograms
        resultsSxx = []
        audio_segment_counter = 1

        i = 0 #(audio_segment_length // 2)
        while i < n_samples:
            audio_sample = audioSignal[slice(i, i + audio_segment_length)]
            f, t, Sxx = signal.spectrogram(transpose(audio_sample), sampling_rate, window=hamming_window,
                                           nperseg=hamming_window_size)
            resultsSxx.append(Sxx)

            audio_segment_counter += 1
            i += audio_segment_length

        return resultsSxx



    def extractAudioFeaturesFromFile(self, file):
        """ returns audio features (spectrograms) for a given audiofile
            using object parameters: framerate, hamming window size
            input: path to file
        """
        # read audio file
        sampling_rate, audioSignal = wavfile.read(file)

        # cast from stereo to mono
        if (len(audioSignal.shape) == 2):
            audioSignal = (audioSignal[:, 0] / 2) + (audioSignal[:, 1] / 2)

        # get dimensions
        n_samples = audioSignal.shape[0]
        audioDuration = n_samples / sampling_rate
        n_audio_segments = math.floor(audioDuration * self.framerate)
        audio_segment_length = math.floor(n_samples / n_audio_segments)

        # set up hamming window
        hamming_window_size = (math.floor(sampling_rate / self.hammingwindowsize))
        hamming_window = signal.hamming(hamming_window_size)

        # get spectrograms
        resultsSxx = []
        audio_segment_counter = 1
        i = 0  # (audio_segment_length // 2)
        while i < n_samples:
            audio_sample = audioSignal[slice(i, i + audio_segment_length)]
            f, t, Sxx = signal.spectrogram(transpose(audio_sample), sampling_rate, window=hamming_window, nperseg=hamming_window_size)
            resultsSxx.append(Sxx)
            audio_segment_counter += 1
            i += audio_segment_length


        return resultsSxx

