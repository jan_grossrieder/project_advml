# This script applies randomized PCA (sklearn) on features stored in a
# HDF5 file to reduce the dimensionality. Be aware that it might use
# heaps of RAM if the dataset is fairly large.
#
# Note that this script is obsolete.
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

import numpy as np
from sklearn.decomposition import PCA
import os
import h5py

# -- SETTINGS
in_file_name = 'features/vgg16.h5'
out_file_name = 'features/vgg16_PCA.h5'
nbr_pca_components = 100

inDirFull = os.path.join(os.getcwd(), in_file_name)
outDirFull = os.path.join(os.getcwd(), out_file_name)

# only in dir non-empty allowed
if not os.path.isfile(inDirFull):
    raise OSError('HDF5 file to be loaded does not exist!')

# initialize hdf5 files
hdInFile = h5py.File(inDirFull, 'r')

# create the numpy array where we temporarily store the VGG16 features
nbrSeq = len(hdInFile)
nbrFrames = hdInFile['0000'].shape[0]
y_dim = nbrSeq * nbrFrames
x_dim = hdInFile['0000'].shape[1]
vggFeature = np.zeros((y_dim, x_dim), dtype=np.float32)

# iterate through datasets and flatten it
startIdx = 0
for idx, key in enumerate(sorted(hdInFile)):
    if idx % 100 == 0:
        print('- flatten dataset, sequence: ' + str(idx + 1) + '/' + str(nbrSeq))
    endIdx = startIdx + hdInFile[key].shape[0]
    vggFeature[startIdx:endIdx, :] = hdInFile[key]
    startIdx = endIdx

# apply PCA
print('- Apply PCA...')
pca = PCA(n_components=nbr_pca_components, svd_solver='randomized')
vggPCA = pca.fit_transform(vggFeature)
print(pca.get_params())
print('any nonzero = ' + str(np.any(vggPCA)))

# convert flatten list back to sequences
hdOutFile = h5py.File(outDirFull, 'w')

# iterate through datasets and save it in output
startIdx = 0
for idx, key in enumerate(sorted(hdInFile)):
    if idx % 100 == 0:
        print('- save to hdf5 file, sequence: ' + str(idx + 1) + '/' + str(nbrSeq))
    endIdx = startIdx + hdInFile[key].shape[0]
    seq = hdOutFile.create_dataset(key, (hdInFile[key].shape[0], nbr_pca_components), dtype='f')
    seq[...] = vggPCA[startIdx:endIdx, :]
    startIdx = endIdx
    hdOutFile.flush()

print('- close files')
hdInFile.close()
hdOutFile.close()

