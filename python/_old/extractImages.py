# Simple script which takes the video-clips and stores the frames in a
# individual folder as .png files.
#
# Note that this script is obsolete and based on the raw images stored as
# .png instead of using the class "extractSamples.py".
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

import os
import moviepy.editor as mpe
import scipy.misc as scm
from progressbar import Bar, Percentage, ProgressBar

# -- SETTINGS
in_directory_name = 'VideoCropPy_out'
out_directory_name = 'features/raw_images'
fps = 15


# -- IMPLEMENTATION
# initialize input directory
inDirFull = os.path.join(os.getcwd(), in_directory_name)

# initialize output directory
out_directory = os.path.join(os.getcwd(), out_directory_name)
if not os.path.exists(out_directory):
    os.makedirs(out_directory)

# only empty output dirs allowed
if os.listdir(out_directory) != []:
    raise OSError('Output directory needs to be empty!')

# read all mp4 files in directory
cnt = 0
dirSorted = sorted(os.listdir(inDirFull))
pbar = ProgressBar(widgets=[Percentage(), Bar()], maxval=len(dirSorted)).start()
for fileIdx, file in enumerate(dirSorted):
    if file.endswith(".mp4"):
        tmpOutFileNbrDir = os.path.join(out_directory, '%04d' % cnt)
        os.makedirs(tmpOutFileNbrDir)

        # get video
        vidClip = mpe.VideoFileClip(os.path.join(inDirFull, file))
        for idx, frame in enumerate(vidClip.iter_frames(fps=fps)):
            scm.imsave(os.path.join(tmpOutFileNbrDir, ('%04d' % idx) + '.png'), frame)

        cnt += 1
    pbar.update(fileIdx)
pbar.finish()

