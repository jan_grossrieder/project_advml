# simple script to check if video-files in "./VideoCropPy_out" contains audio and if not, move it to "./noAudio"

import os
import moviepy.editor as mpe

videodir = './VideoCropPy_out'

falsedir = './noAudio'

if not os.path.isdir(falsedir):
    os.mkdir(falsedir)

vid_list = os.listdir(videodir)

for i, vid_name in enumerate(vid_list):
    print('process video %d/%d' % (i+1, len(vid_list)))

    vid_dir = os.path.join(videodir, vid_name)

    vidClip = mpe.VideoFileClip(vid_dir)

    try:
        tmp = vidClip.audio.subclip(0, 1)
    except:
        print('delete file %s' % vid_name)
        move_dir = os.path.join(falsedir, vid_name)
        os.rename(vid_dir, move_dir)
