# Extracts audio from video files
# Advanced Machine Learning Project
# author: Tobias Rothen

# get ffmpeg (uncomment on first run)
#import imageio
#imageio.plugins.ffmpeg.downl   oad()

# imports
import os
from moviepy.editor import *

# -- SETTINGS
debug = True # prints verbose info about each file
out_directory_name = 'audio-files'


# -- IMPLEMENTATION

# init running counters
noAudioCounter = 0
storedAudioCounter = 0

# initialize output directory
out_directory = './' + out_directory_name
if not os.path.exists(out_directory):
    os.makedirs(out_directory)

# read all mp4 files in directory
for file in os.listdir("./"):
    if file.endswith(".mp4"):
        if debug:
            print('DEBUG: extracting audio from file', file)
        # get audio
        vidClip = VideoFileClip(file)
        audioClip = vidClip.audio

        if audioClip is not None:
            # store
            audio_filename = out_directory + "/" + file.replace(".mp4", "") + "_audio.wav"
            audioClip.write_audiofile(audio_filename)
            storedAudioCounter += 1
        else:
            noAudioCounter += 1
            if debug:
                print('DEBUG: extracting audio from file', file)

print('')
print('== [VideoCropPy] AudioSplit Results: ')
print('A total of', storedAudioCounter, 'audio files have been stored to', out_directory_name)
print('A total of', noAudioCounter, 'video files did not contain any audio')
print('Therefore ', (noAudioCounter / (storedAudioCounter + noAudioCounter)) * 100, '% of the video data was dropped!')
