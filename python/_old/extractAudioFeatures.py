# Extracts audio features (spectrograms) from a given audio file
# Advanced Machine Learning Project
# author: Tobias Rothen

# imports
import os
from scipy import signal
from scipy.io import wavfile
from matplotlib.mlab import PCA
from numpy import ndarray
from numpy import transpose
import math

# -- SETTINGS
audio_directory_name = 'audio-files'

# Sample expectation
FRAMERATE = 15

# -- IMPLEMENTATION


# - Option A: read data from audio files
#filecounter = 0
# read all audiofiles
#for file in os.listdir("./"):
#    if file.endswith(".wav"):
#        filecounter += 1

# - Option B: read only one file manually [TESTING]
file = audio_directory_name + '/example_vid_03_audio.wav'


# ------- 0) read file
sampling_rate, audioSignal = wavfile.read(file)

# cast from stereo to mono
if (len(audioSignal.shape) == 2):
    audioSignal = (audioSignal[:,0] / 2) + (audioSignal[:,1] / 2)

n_samples = audioSignal.shape[0]
audioDuration = n_samples / sampling_rate

print('DEBUG: audio dimension: ', audioSignal.shape)
print('DEBUG: audio sampling rate: ', sampling_rate)
print('DEBUG: audio duration: ', audioDuration)

# ------- 1) define segments

# Extracting the audio samples in 2s samples with 15 fps:
# whereas [1t =  2s/30]
#  t:  0         (frame1)      2       3(frame2)   4   .. etc.      28         29 (frame30)       30
#      |---------(audio1)------|--------(audio2)----|                |------------(audio30)--------|

n_audio_segments = math.floor(audioDuration * FRAMERATE)
audio_segment_length = math.floor(n_samples / n_audio_segments)


# ------- 2) define hamming window

# Principle is well described here:
# https://youtu.be/qrU2jsSqWD8?t=1649

# 2.1) size defined as 20ms with a 10ms overlap, as suggested in the given papers:
# https://people.csail.mit.edu/khosla/papers/icml2011_ngiam.pdf
# https://arxiv.org/pdf/1701.04224.pdf
# (> parameters focus on speech recognition, might not be optimal)
# one timepoint = SAMPLETIME * sample_rate
                                            # results in the following spectogram dimensionalities:
t_20ms = math.floor(sampling_rate / 50)     #  time 3 / freq. 442
t_10ms = math.floor(sampling_rate / 100)    #  time 7 / freq. 221
t_5ms = math.floor(sampling_rate / 200)     #  time 15 / freq. 111

hamming_window_size = t_10ms
hamming_window = signal.hamming(hamming_window_size)

# --> results in:       time-resolution: 5
#                       freq-resolution: 442

# 2.2)
# try out other hamming window parameters
# (bigger window --> higher time res., lower freq. res.)
# TODO


# ------- 3) loop through audio segments and get spectrogram
resultsSxx = []
resultsf = []
resultst = []
audio_segment_counter = 1
i = 0
while i < n_samples:
    audio_sample = audioSignal[slice(i, i + audio_segment_length)]

    # get spectrogram
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html#scipy.signal.spectrogram
    f, t, Sxx = signal.spectrogram(transpose(audio_sample), sampling_rate, window=hamming_window, nperseg=hamming_window_size)

    # store results
    resultsSxx.append(Sxx)
    resultsf.append(f)
    resultst.append(t)

    audio_segment_counter += 1
    i += audio_segment_length

# ------- 4) apply principal components analysis (PCA) to reduce the dimensionality
# TODO




