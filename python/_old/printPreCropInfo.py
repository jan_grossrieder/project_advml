# Collects information about video files in the given folder
# Outputs estimation on how many samples can be generated from the video data
# Advanced Machine Learning Project
# author: Tobias Rothen

# get ffmpeg (uncomment on first run)
#import imageio
#imageio.plugins.ffmpeg.download()

# imports
import os
from moviepy.editor import *

# -- SETTINGS
crop_time = 10  # size of video crops in seconds
frame_resolution = 64 # resolution of target vid
debug = True # prints verbose info about each file


# -- IMPLEMENTATION
# print some information about the directory
fileCount = 0
sequenceCount = 0
totalLength = 0
totalSize = 0
totalShortClips = 0
totalLowQualityClips = 0
print('')
print ('======= [VideoCropPy] Printing PreCrop Infos')
print('')
if debug:
    print('= DEBUG output:')
    print('')

for file in os.listdir("./"):
    if file.endswith(".mp4"):
        fileCount += 1

        # get info from file
        vidClip = VideoFileClip(file)
        totalLength += vidClip.duration
        sequenceCount += int(vidClip.duration / crop_time)
        # TODO: get filesize

        # check if reformat is required
        if vidClip.size[0] == vidClip.size[1]:
            reformat = False
        else:
            reformat = True

        # check if file duration is too small
        if vidClip.duration < crop_time:
            totalShortClips += 1

        # check if resolution is too small
        if (vidClip.size[0] < frame_resolution) or (vidClip.size[1] < frame_resolution):
            totalLowQualityClips += 1

        if debug:
            print("DEBUG: File found: ", file, " (duration: ", vidClip.duration, ", fps:", vidClip.fps, ")")
            if reformat:
                print("DEBUG: File ", file, " has non-quadratic image dimensions: ", vidClip.size[0], "/", vidClip.size[1], " and must be resized!")

# print info
print('')
print ('== Files found:')
print('Files found with .mp4 ending: ', fileCount)
#print('Total size (all files): ')
print('Total length (all files): ', totalLength, " seconds")
print('')
print('= Potential result after cropping:')
print('Total sequences after cropping: ', sequenceCount)
print('Average Information Content (seq. / file)', sequenceCount / fileCount)
print('')
print('= Unused video material')
print('Percentage of files used:', 100.0 * ((fileCount - (totalLowQualityClips + totalShortClips)) / fileCount),'%')
print('Total Unused files: ', totalShortClips + totalLowQualityClips)
print('Total files with too small duration: ', totalShortClips)
print('Total files with too small resolution: ', totalLowQualityClips)
print('============================================================')
print('')