# VGG-extraction from raw images stored in 'features/raw_images'. It uses
# the framework provided by https://github.com/fchollet/deep-learning-models
#
# Note that this script is obsolete and based on the raw images stored as
# .png instead of using the class "extractSamples.py".
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

import numpy as np
import scipy.misc as scm
import os
from keras.models import Model
from DLM.vgg16 import VGG16
from DLM.imagenet_utils import preprocess_input
import h5py

# -- SETTINGS
in_directory_name = 'features/raw_images'
out_file_name = 'features/vgg16.h5'

# -- MODEL DEFINITION
base_model = VGG16(weights='imagenet')
model = Model(input=base_model.input, output=base_model.get_layer('fc2').output)

inDirFull = os.path.join(os.getcwd(), in_directory_name)
outDirFull = os.path.join(os.getcwd(), out_file_name)

groupInList = sorted(os.listdir(inDirFull))
nbrSeq = len(groupInList)

# only in dir non-empty allowed
if nbrSeq == 0:
    raise OSError('Output directory needs to be empty!')

# get number of frames per sequence (based on seqeunce 0)
nbrFrames = len(os.listdir(os.path.join(inDirFull, groupInList[0])))

# initialize hdf5 file
hdFile = h5py.File(outDirFull, "w")

# iterate through groups (sequences)
for idxSeq, groupDir in enumerate(groupInList):
    tmpDirFullPath = os.path.join(inDirFull, groupDir)

    # create the dataset for this specific sequence
    seq = hdFile.create_dataset(groupDir, (nbrFrames, 4096), dtype='f')

    # iterate through frames in sequence and read the frames
    frameList = list()
    for idxFrame, imgName in enumerate(sorted(os.listdir(tmpDirFullPath))):
        print('- processing sequence ' + str(idxSeq + 1) + '/' + str(nbrSeq) +
              ', image ' + str(idxFrame + 1) + '/' + str(nbrFrames))

        # read the image
        img = scm.imread(os.path.join(tmpDirFullPath, imgName), mode='RGB')

        # resize it to have correct resolution
        x = scm.imresize(img, (224, 224)).astype('float64')

        # get feature
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        seq[idxFrame, :] = model.predict(x)[0]

    hdFile.flush()

hdFile.close()

