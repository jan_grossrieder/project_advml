# Simple script which takes the two second random audio samples from the test
# set stored in a HDF5 file (see class extractSamples.py) together with the
# reconstructed video-frames stored in "yhat.h5" and convert it to mp4
# video-clips.
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

import h5py
import numpy as np
import os
from extractSamples import extractSamples
import moviepy.editor as mpe
import scipy.misc as scm

# TODO: adapt this directory
inDirVideoFull = '../nn/yhat.h5'
outDirFull = './reconstructed_clips'
outDirTmp = './reconstructed_clips/tmp'

if not os.path.isdir(outDirFull):
    os.mkdir(outDirFull)

if not os.path.isdir(outDirTmp):
    os.mkdir(outDirTmp)

es = extractSamples()
nbr_samples = es.get_nbr_samples()
split_idx = int(0.8*nbr_samples)

h5_in = h5py.File(inDirVideoFull, 'r')

for i, idx in enumerate(range(split_idx, nbr_samples)):
    print('get test sample %i/%i' % (i + 1, nbr_samples - split_idx))
    audioSig = es.get_audio_sample(idx)
    frames = h5_in['yHat_%05d' % i][...]

    for ii, a in enumerate(frames):
        scm.imsave(os.path.join(outDirTmp, '%02d.png' % ii), np.repeat(a[:,:, np.newaxis], 3, axis=2))

    myList = [os.path.join(outDirTmp, a) for a in sorted(os.listdir(outDirTmp))]

    clip = mpe.ImageSequenceClip(myList, fps=15)
    clip.audio = audioSig
    clip.write_videofile(os.path.join(outDirFull, '%05d.mp4' % i))
