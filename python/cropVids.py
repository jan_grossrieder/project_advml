# Crops all videos in the folder (aspect ratio correction, rescaling, duration cropping)
# Advanced Machine Learning Project
# author: Tobias Rothen

# get ffmpeg (uncomment on first run)
#import imageio
#imageio.plugins.ffmpeg.download()

# imports
from moviepy.editor import *
from moviepy.video import fx
import os

# -- SETTINGS
crop_time = 20
resolution = 64
frame_rate = 0  # set 0 if it should be left the same
out_directory_name = 'VideoCropPy_out'
debug = True


# -- IMPLEMENTATION

# initialize output directory
out_directory = './' + out_directory_name
if not os.path.exists(out_directory):
    os.makedirs(out_directory)

# store some information about the videos that could not be used
lowResCounter = 0
lowFPSCounter = 0
lowDurationCounter = 0
seqCounter = 0
fileCounter = 0
# read all mp4 files in directory
for file in os.listdir("./"):
    if file.endswith(".mp4"):
        fileCounter += 1
        failed = False
        vidClip = VideoFileClip(file)
        vidClip_width = vidClip.size[0]
        vidClip_height = vidClip.size[1]

        if debug:
            print('DEBUG: Cropping file: ', vidClip.filename)

        # apply reformat of width/height (if necessary)
        if not(vidClip_width == vidClip_height):
            vidCenterX = int(vidClip_width / 2)
            vidCenterY = int(vidClip_height / 2)

            if vidClip_width > vidClip_height:
                # crop left + right
                vidClip = fx.all.crop(vidClip, x_center=vidCenterX, y_center=vidCenterY,
                                      height=vidClip_height, width=vidClip_height)

            else:
                # crop top +  bottom
                vidClip = fx.all.crop(vidClip, x_center=vidCenterX, y_center=vidCenterY,
                                      height=vidClip_width, width=vidClip_width)

        # resize clip and change frame rate
        if(vidClip_height < resolution) or (vidClip_width < resolution):
            # not high enough resolution
            failed = True
            lowResCounter += 1
        else:
            # resize
            vidClip = vidClip.resize(width=resolution)

        if vidClip.fps < frame_rate:
            failed = True
            lowFPSCounter += 1
        else:
            if not (frame_rate == 0):
                vidClip.set_fps(frame_rate)

        # split up into sequences and store in out folder
        if not failed:
            if vidClip.duration < crop_time:
                lowDurationCounter += 1
            else:
                no_sequences = int(vidClip.duration / crop_time)
                if vidClip.duration % crop_time == 0 :
                    vid_start = 0
                    vid_end = vidClip.duration

                else:
                    delta = vidClip.duration / crop_time
                    if delta % 2 == 0:
                        # even delta, cut the beginning and end equally
                        vid_start = int(crop_time / 2)
                        vid_end = int(vidClip.duration) - int(crop_time / 2)

                    else:
                        # odd delta, cut 1 more second at the beginning of the video
                        vid_start = int((crop_time + 1) / 2)
                        vid_end = int(vidClip.duration) - int((crop_time - 1) / 2)

                for seq in range(0, no_sequences):
                    seq_filename = out_directory + "/" + file.replace(".mp4", "") + "_seq" + str(seq) + ".mp4"
                    subClip = vidClip.subclip(seq * crop_time, (seq + 1) * crop_time)
                    subClip.write_videofile(seq_filename, codec="libx264", audio=True)
                    seqCounter += 1


# Issues
# - fps causes long exec. time
# - no audio with video-players that do not support libx264 encoder (e.g. QuickTime Player) --> works with VLC

# print results
print('')
print('== [VideoCropPy] Results: ')
print('A Total of', seqCounter, 'sequences created in folder', out_directory_name)
print('Files processed:', 100.0 * ((fileCounter - (lowDurationCounter + lowFPSCounter + lowResCounter)) / fileCounter), '%')
print('Total video files untouched:', (lowDurationCounter + lowFPSCounter + lowResCounter))
print('Videos skipped due to low resolution:', lowResCounter)
print('Videos skipped due to low FPS:', lowFPSCounter)
print('Videos skipped due to low duration:', lowDurationCounter)
print('============================================================')
print('')

