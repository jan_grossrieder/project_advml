# Simple script which takes the two second random samples from the original 
# test set stored in a HDF5 file (see class extractSamples.py) and convert
# it to a mp4 video-clip.
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

import h5py
import numpy as np
import os
from extractSamples import extractSamples
import moviepy.editor as mpe
import scipy.misc as scm

outDirFull = './reconstructed_orig_clips'
outDirTmp = './reconstructed_orig_clips/tmp'

if not os.path.isdir(outDirFull):
    os.mkdir(outDirFull)

if not os.path.isdir(outDirTmp):
    os.mkdir(outDirTmp)

es = extractSamples(is_rgb=False)
nbr_samples = es.get_nbr_samples()
split_idx = int(0.8*nbr_samples)

for i, idx in enumerate(range(split_idx, nbr_samples)):
    print('get test sample %i/%i' % (i + 1, nbr_samples - split_idx))
    audioSig = es.get_audio_sample(idx)
    frames = es.get_video_sample(idx)

    for ii, a in enumerate(frames):
        scm.imsave(os.path.join(outDirTmp, '%02d.png' % ii), np.repeat(a[:,:, np.newaxis], 3, axis=2))

    myList = [os.path.join(outDirTmp, a) for a in sorted(os.listdir(outDirTmp))]

    clip = mpe.ImageSequenceClip(myList, fps=15)
    clip.audio = audioSig
    clip.write_videofile(os.path.join(outDirFull, '%05d.mp4' % i))

