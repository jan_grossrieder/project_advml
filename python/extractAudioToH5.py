# This script makes use of the class "extractSamples.py" and retrieves
# 2 second audio samples. It then computes the spectrogram using the
# class "AudioFeatureExtractor.py", split the data into 80% train and
# 20% test data and stores it in a HDF5-file.
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

from extractSamples import extractSamples
from AudioFeatureExtractor import AudioFeatureExtractor
import h5py
import numpy as np

outDirFull = './features/audio_feat.h5'

es = extractSamples()
afe = AudioFeatureExtractor()

hd_out = h5py.File(outDirFull, 'w')
nbr_samples = es.get_nbr_samples()
split_idx = int(0.8*nbr_samples)

# get train set
dim = (split_idx, 30, 221, 7)

seq = hd_out.create_dataset('train', dim, dtype='float32')
for idx in range(split_idx):
    print('get train sample %i/%i' % (idx + 1, split_idx))

    # convert to numpy array
    tmpArr = np.zeros((30,221,7)).astype('float32')
    for ii, arr in enumerate(afe.extractAudioClipFeatures(es.get_audio_sample(idx))):
        tmpArr[ii,...] = arr

    seq[idx, ...] = tmpArr
    if (idx % 100) == 99:
        hd_out.flush()
        print('-> flushed')
hd_out.flush()
print('-> flushed')

# get test set
dim = (nbr_samples - split_idx, 30, 221, 7)

seq = hd_out.create_dataset('test', dim, dtype='float32')
for i, idx in enumerate(range(split_idx, nbr_samples)):
    print('get test sample %i/%i' % (i + 1, nbr_samples - split_idx))

    # convert to numpy array
    tmpArr = np.zeros((30, 221, 7)).astype('float32')
    for ii, arr in enumerate(afe.extractAudioClipFeatures(es.get_audio_sample(idx))):
        tmpArr[ii, ...] = arr

    seq[i, ...] = tmpArr

    if (i % 100) == 99:
        hd_out.flush()
        print('-> flushed')
hd_out.flush()
print('-> flushed')

hd_out.close()

