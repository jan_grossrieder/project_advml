# This class retrieves audio and video samples of length 2s out of 20s video-clips.
# The samples are randomly shuffled and a pickle file is stored to ensure
# reproducebility.
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

"""
HOW TO USE:

from extractSamples import extractSamples

es = extractSamples()

nbr_of_samples = es.get_nbr_samples()
audio_sample = es.get_audio_sample(0)  # get audio sample 0
video_sample = es.get_video_sample(0)  # get video sample 0
"""

import os
import numpy as np
import random as rd
import pickle
import moviepy.editor as mpe


class extractSamples(object):
    def __init__(self, videodir='./VideoCropPy_out', fps=15, t_max=20, sample_time=2, t_list_dir='./t_list.pk', is_rgb=True):
        self.videodir = videodir
        self.fps = fps
        self.tSample = sample_time
        self.tMax = t_max
        self.vidList = [os.path.join(videodir, i) for i in sorted(os.listdir(videodir))]
        self.isRGB = is_rgb

        tmp = np.arange(1/(2*self.fps), t_max, 1/self.fps)

        if os.path.isfile(t_list_dir):
            with open(t_list_dir, 'rb') as fp:
                self.tList = pickle.load(fp)
            return

        tmp_list = list()
        nbrFramesPerSample = sample_time*fps
        for i in range(t_max // sample_time):
            i_s = i*nbrFramesPerSample
            i_e = (i+1)*nbrFramesPerSample
            timestamps = np.array([i*sample_time, (i+1)*sample_time])
            tmp_list.append(np.concatenate((timestamps, tmp[range(i_s, i_e)])))
            i_s += (nbrFramesPerSample // 2)
            i_e += (nbrFramesPerSample // 2)
            if i_e <= (fps*t_max):
                timestamps = np.array([i*sample_time + (sample_time / 2), (i+1)*sample_time + (sample_time / 2)])
                tmp_list.append(np.concatenate((timestamps, tmp[range(i_s, i_e)])))

        self.tList = list()
        cnt = 0
        for i in self.vidList:
            for ii in tmp_list:
                self.tList.append(list())
                self.tList[cnt].append(i)
                self.tList[cnt].append(ii[:2])
                self.tList[cnt].append(ii[2:])
                cnt += 1

        rd.seed(1990)
        rd.shuffle(self.tList)

        with open(t_list_dir, 'wb') as fp:
            pickle.dump(self.tList, fp)


    def get_nbr_samples(self):
        return len(self.tList)


    def get_audio_sample(self, sample_idx):
        dir = self.tList[sample_idx][0]
        times = self.tList[sample_idx][1]

        vidClip = mpe.VideoFileClip(dir)
        return vidClip.audio.subclip(times[0], times[1])


    def get_video_sample(self, sample_idx):
        dir = self.tList[sample_idx][0]

        vidClip = mpe.VideoFileClip(dir)
        sz = vidClip.size

        if self.isRGB == False:
            ret_arr = np.zeros((self.tSample * self.fps, sz[0], sz[1])).astype('uint8')
        else:
            ret_arr = np.zeros((self.tSample*self.fps, sz[0], sz[1], 3)).astype('uint8')

        for idx, item in enumerate(self.tList[sample_idx][2]):
            tmp = vidClip.get_frame(item)
            if self.isRGB == False:
                tmp = rgb2gray(tmp)
            ret_arr[idx, ...] = tmp

        return ret_arr

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114]).astype('uint8')
