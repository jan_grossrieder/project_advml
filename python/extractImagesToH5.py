# This script makes use of the class "extractSamples.py" and retrieves
# 2 second video samples. It then splits those already shuffled samples
# into 80% train and 20% test data and stores it in a HDF5-file.
#
# Maintainer:    Jan Grossrieder
# Last modified: May 30th 2017

from extractSamples import extractSamples
import h5py

outDirFull = './features/im_feat_gray.h5'
is_rgb = False

es = extractSamples(is_rgb=is_rgb)

hd_out = h5py.File(outDirFull, 'w')
nbr_samples = es.get_nbr_samples()
split_idx = int(0.8*nbr_samples)

# get train set
if is_rgb == False:
    dim = (split_idx, 30, 64, 64)
else:
    dim = (split_idx, 30, 64, 64, 3)

seq = hd_out.create_dataset('train', dim, dtype='uint8')
for idx in range(split_idx):
    print('get train sample %i/%i' % (idx + 1, split_idx))
    seq[idx, ...] = es.get_video_sample(idx)
    if (idx % 100) == 99:
        hd_out.flush()
        print('-> flushed')
hd_out.flush()
print('-> flushed')

# get test set
if is_rgb == False:
    dim = (nbr_samples - split_idx, 30, 64, 64)
else:
    dim = (nbr_samples - split_idx, 30, 64, 64, 3)
seq = hd_out.create_dataset('test', dim, dtype='uint8')
for i, idx in enumerate(range(split_idx, nbr_samples)):
    print('get test sample %i/%i' % (i + 1, nbr_samples - split_idx))
    seq[i, ...] = es.get_video_sample(idx)

    if (i % 100) == 99:
        hd_out.flush()
        print('-> flushed')
hd_out.flush()
print('-> flushed')

hd_out.close()

