Source code is divided into two folders:
- 'python': containing python scripts for preprocessing and postprocessing
- 'nn': containing neural nets implemented in torch

--------------
Python-Scripts
--------------
1. Requirements (python3):
   - moviepy
   - h5py
   - pickle

2. Preprocessing:
   - run the script 'extractAudioToH5.py' to extract 2s audio-features from
     the 20s videos and store them in a hdf5 file (python/features/audio_feat.h5)
   - run the script 'extractImagesToH5.py' to extract 2s video-features from
     the 20s videos and store them in a hdf5 file (python/features/im_feat_gray.h5)

3. Postprocessing:
   - once you trained the network (see below nn-Scripts) run script 'saveClipFromH5.py'
     to convert the predicted frames back to a mmpeg together with the real audio
     (stored in 'python/reconstructed_clips/')

----------
nn-Scripts
----------
1. Requirements:
   - cunn
   - hdf5
   - cutorch

2. Train the Audio-convLSTM (described in report, section 2.3.3):
   - Run the script 'audioLSTM.lua', which will take the datasets generated in the
     preprocess step and predict video and stores it in 'yhat.h5'
   - losses are stored in 'loss.txt'

3. (Video-LSTM):
   - script 'VideoconvLSTM.lua' implements the video-autoencoder from model described
     in section (2.3.2 convLSTM and Autoencoder Combined)
   - to run this, please refer to the README.txt in 'nn/'

